const config = {
    verbose: true,
    rootDir: './tests',
    moduleDirectories: ['node_modules', '<rootDir>/src']
};

export default config;