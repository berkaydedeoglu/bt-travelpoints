
/**
 * Parses the Basic HTTP authorization string into plain email and password format.
 * This function parses the provided strings relying RFC-7617.
 * @param {string} headerString -> HTTP basic authorization value in string type
 * @returns {(Array<string | null>} -> An ordered array that includes user email
 *                                      related plain-text password. On any unexpected
 *                                      case, the function returns a null array
 */
export function parseBasicAuthHeader (headerString) {
    const splittedHeaderString = headerString.split(' ')
    if (splittedHeaderString.length !== 2) { return [null, null] }

    // Bearer <token>   ->   <token>
    const credentialToken = splittedHeaderString[1]

    const decodedToken = Buffer.from(credentialToken, 'base64').toString()
    if (!decodedToken) { return [null, null] }

    // TODO: Handle if password or email includes char ':'
    const splittedToken = decodedToken.split(':')
    return splittedToken.length === 2 ? splittedToken : [null, null]
}

export function scrapeJWTTokenFromHeader (headerString) {
    const splittedHeaderString = headerString.split(' ')
    if (splittedHeaderString.length !== 2) { return [null, null] }

    return splittedHeaderString[1]
}
