import jwt from 'jsonwebtoken'

/**
 * Creates an expiry date epoch value from current time according to
 * environment values.
 *
 * @returns {Number} -> Calculated epoch value in seconds unit
 */
function generateExpiryDate () {
    const epochInSeconds = Math.floor(Date.now() / 1000)
    const expiringTime = Number(process.env.JWT_EXPIRING_TIME)
    return epochInSeconds + expiringTime
}

/**
 * Signs the provided object to jwt. We used
 * HS256 algorithm and relied the HTTPS security.
 *
 * @param {Object} tokenData -> an object to patch via jwt
 * @returns {(string | null)} -> the jwt string.
 */
function signTokenData (tokenData) {
    tokenData.exp = generateExpiryDate()
    try {
        return jwt.sign(tokenData, process.env.JWT_SECRET)
    } catch {
        return null
    }
}

/**
 * Creates a new jwt token for a provided user.
 *
 * @param {models.user} user -> the user model for providing
 *                              a jwt token
 * @returns {(string | null)} -> the token string if successfully
 *                              created else null
 */
export function createTokenForUser (user) {
    return signTokenData({ email: user.email })
}

/**
 * A wrapper function for jwt token verification.
 * @param {string} tokenString -> jwt token string
 * @returns {object} -> a response object that created for verification
 *                      verified: the token verified succesfully or not
 *                      data: the data of provided token.
 *                      errorMessage: an error mesage if an error occured
 */
function verifyToken (tokenString) {
    let verifiedTokenData
    try {
        verifiedTokenData = jwt.verify(tokenString, process.env.JWT_SECRET)
    } catch (err) {
        return {
            verified: false,

            // XXX: maybe i can use an message wraper
            // becauce throwing the message directly may
            // be caused security vulnerability.
            errorMessage: err.message
        }
    }

    return {
        verified: true,
        data: verifiedTokenData
    }
}

/**
 * Renews JWT token if provided token is valid.
 * @param {string} tokenString -> Jwt string which will be renewed
 * @returns {(Object | null)} -> an response object or null
 *                              token: jwt string
 *                              data: jwt session body
 */
export function renewToken (tokenString) {
    const verificationResponse = verifyToken(tokenString)
    if (!verificationResponse.verified || !verificationResponse.data) {
        console.log(verificationResponse.errorMessage)
        return null
    }

    const newToken = signTokenData(verificationResponse.data)
    if (!newToken) { return null }

    return { token: newToken, data: verificationResponse.data }
}
