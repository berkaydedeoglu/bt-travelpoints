import { renewToken } from '../jwtHelper'
import { scrapeJWTTokenFromHeader } from '../httpHelper'

const EXCLUDED_PATHS = ['/auth/login', '/auth/register']
const ERROR_OBJECT = { error: true, message: 'Authetication Required' }

function isPathExcluded (path) {
    return !!EXCLUDED_PATHS.find(element => element === path)
}

export default (req, res, next) => {
    if (isPathExcluded(req.path)) { return next() }
    const authHeader = req.get('Authorization')
    if (!authHeader) {
        return res.status(406).json(ERROR_OBJECT)
    }

    const jwtToken = scrapeJWTTokenFromHeader(authHeader)
    if (!jwtToken) {
        return res.status(406).json(ERROR_OBJECT)
    }

    const tokenResponse = renewToken(jwtToken)
    if (!tokenResponse) {
        return res.status(406).json(ERROR_OBJECT)
    }

    req.sessionData = tokenResponse.data
    const httpHeaderString = `Bearer ${tokenResponse.token}`
    res.set('Authorization', httpHeaderString)
    return next()
}
