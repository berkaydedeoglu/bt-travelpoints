/**
 * @typedef GeoLocationPoint
 * @property {number} lat -> latitude value in degrees
 * @property {number} lon -> longtitude value in degrees
 */

// the radius of the sphere
const EARTH_RADIUS = 6378.1

/**
 * Converts radians to degrees
 * @param {number} radians -> radians value to be converted
 * @returns {number} -> the angle in degrees
 */
function radians2Degrees (radians) {
    return radians * (180 / Math.PI)
}

/**
 * Converts degrees to radians
 * @param {number} degrees -> degrees value to be converted
 * @returns {number} -> the angle in radians
 */
function degrees2Radians (degrees) {
    return degrees * (Math.PI / 180.0)
}

function roundFloat (floatNumber, floatingPoint) {
    return +(Math.round(floatNumber + 'e+' + floatingPoint) + 'e-' + floatingPoint)
}

/**
 * Calculates spherical distance according to Haversine Formula.
 * Note: I could also use Vincenty formula if i need more accuracy.
 *      But haversine formula is accurate enough for our approach.
 * @param {GeoLocationPoint} sourceLocation -> First location (from)
 * @param {GeoLocationPoint} targetLocation -> Second location (to)
 * @returns {number} -> spherical distance
 */
function haversineDistance (sourceLocation, targetLocation) {
    let dLat = targetLocation.lat - sourceLocation.lat
    dLat = degrees2Radians(dLat)

    let dLon = targetLocation.lon - sourceLocation.lon
    dLon = degrees2Radians(dLon)

    const sourceLat = degrees2Radians(sourceLocation.lat)
    const targetLat = degrees2Radians(targetLocation.lat)

    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(sourceLat) * Math.cos(targetLat) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    const distance = EARTH_RADIUS * c

    return distance
}

/**
 * Calculates destination geolocation vactor according to given distance
 * and bearing from start geolocation.
 *
 * @param {GeoLocationPoint} startingPoint -> the base point (from)
 * @param {number} bearing -> the heading angle of the calculation vector
 *                            in radiant format
 * @param {number} distance -> the distance between startingPoint and desired
 *                              destination point
 * @returns {GeoLocationPoint} -> destionation location of generated vector
 */
function getDestinationFromDistanceAndBearing (startingPoint, bearing, distance) {
    const lat1 = degrees2Radians(startingPoint.lat)
    const lon1 = degrees2Radians(startingPoint.lon)

    const rDistance = distance / EARTH_RADIUS

    let lat2 = Math.asin(Math.sin(lat1) * Math.cos(rDistance) +
        Math.cos(lat1) * Math.sin(rDistance) * Math.cos(bearing))

    let lon2 = lon1 + Math.atan2(Math.sin(bearing) * Math.sin(rDistance) *
        Math.cos(lat1), Math.cos(rDistance) - Math.sin(lat1) * Math.sin(lat2))

    lat2 = radians2Degrees(lat2)
    lon2 = radians2Degrees(lon2)

    lat2 = roundFloat(lat2, 5)
    lon2 = roundFloat(lon2, 5)
    return { lat: lat2, lon: lon2 }
}

/**
 * Generates a virtual rectangle which centered around a base location provided.
 * @param {Object} param
 *                 param.basedLocation {GeoLocationPoint}: starting location
 *                 param.radius: distance from based location in KM
 * @returns {Object} -> an object that contains 4 values which represents
 *                      the edge coordinates of rectangle of base location
 */
export function getHaversineBorders ({ basedLocation, radius }) {
    return {
        left: getDestinationFromDistanceAndBearing(basedLocation, degrees2Radians(270), radius),
        top: getDestinationFromDistanceAndBearing(basedLocation, degrees2Radians(0), radius),
        right: getDestinationFromDistanceAndBearing(basedLocation, degrees2Radians(90), radius),
        bottom: getDestinationFromDistanceAndBearing(basedLocation, degrees2Radians(180), radius)
    }
}

/**
 * Creates a virtual circler which centered by center location and checks
 * the other provided location is in that or not
 * @param {Object} param
 *                 param.center {GeoLocationPoint}: referrence location points
 *                 param.controlLocation {GeoLocationPoint}: the location to be checked
 *                 param.radius: distance from based location in KM
 * @returns {boolean} -> True if the controlLocation is on virtual circle else false
 */
export function isInsideTheCircle ({ center, controlLocation, radius }) {
    return haversineDistance(center, controlLocation) <= radius
}
