import bcrypt from 'bcrypt'
import log from './logHelper'

/**
 * A Decorator of Bcrypt hash function which
 * extends with extra values
 * @param {string} passwd -> password string
 * @returns {Promise} -> Original bcrypt promise
 */
function encryptPasswd (passwd) {
    const saltRound = Number(process.env.SALT_ROUND)
    return bcrypt.hash(passwd, saltRound)
}

export function encryptSafely (passwd) {
    try {
        return encryptPasswd(passwd)
    } catch (err) {
        log.error('An error occured while encypting password.')
        return false
    }
}

export function compareSafely (passwd, hashedPasswd) {
    try {
        return bcrypt.compare(passwd, hashedPasswd)
    } catch (err) {
        log.error('An error occured while comparing password.')
        return false
    }
}
