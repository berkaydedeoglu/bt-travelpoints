import log from './logHelper'

export async function execDBQuery (query) {
    let response
    try {
        response = await query.exec()
    } catch (err) {
        log.error('An error occured on query execution')
    }

    return response
}

export function saveDBModel (model) {
    try {
        model.save()
        return true
    } catch {
        log.error('An error occured on writing to db')
        return false
    }
}
