/**
 * This module contains logger decorators.
 * Maybe i can use a logger library but i have
 * choosed this easy way
 */

/**
 * Generates a formatted string that can be
 * used in logging
 * @returns formatted time string
 *          ex: 10:34
 */
function getFormattedCurrentTime () {
    const time = new Date()
    return `${time.getHours()}:${time.getMinutes()}`
}

/**
 * A logger decorator that appends time token to
 * log message (see decorator pattern)
 * @param {function} func -> a function to be decorated
 * @returns {function} -> Decorated function
 */
function logDecorator (func) {
    return (errorString) => {
        const errorMessage = `[${getFormattedCurrentTime()}] - ${errorString}`
        return func(errorMessage)
    }
}

export default {
    error: logDecorator(console.error),
    info: logDecorator(console.log)
}
