import { isInsideTheCircle } from '../helpers/geolocationHelper'
import { execDBQuery } from '../helpers/dbHelper'
import travelModel from '../models/travel'
import travelModelDecorators from '../models/dbDecorators/travelModelDecorators'

/**
 * @typedef zone
 * @property lat -> Latitude value
 * @property lon -> Longtitude value
 * @property radius -> Radius value of virtual circle in KM
 */

export async function addATravel ({ email, startLocation, endLocation, travelDetails }) {

}

export async function getTravels ({ zone, timeInterval, groupByCarModelYear, distancePeaks }) {
    const operations = generateTravelAggregationOperations({
        zone: zone,
        timeInterval: timeInterval,
        groupByCarModelYear: groupByCarModelYear,
        distancePeaks: distancePeaks
    })

    const query = travelModel.aggregate(operations)

    let travels = await execDBQuery(query)
    if (!travels) { return false }

    if (!groupByCarModelYear && !distancePeaks) {
        travels = filterForMoreAccuracy(travels, zone)
    }

    return travels
}

function filterForMoreAccuracy (travels, referenceZone) {
    return travels.filter((travel) => {
        return isInsideTheCircle({
            center: { lat: referenceZone.lat, lon: referenceZone.lon },
            controlLocation: travel.startingPoint,
            radius: referenceZone.radius
        })
    })
}

function generateTravelAggregationOperations ({ zone, timeInterval, groupByCarModelYear, distancePeaks }) {
    let aggregationOperations = []

    aggregationOperations = travelModelDecorators
        .zoneOperation(aggregationOperations, zone)

    if (groupByCarModelYear) {
        aggregationOperations = travelModelDecorators
            .carGroupOperation(aggregationOperations)
    }

    if (timeInterval) {
        aggregationOperations = travelModelDecorators
            .timeIntervalOperation(aggregationOperations, timeInterval)
    }

    if (distancePeaks) {
        aggregationOperations = travelModelDecorators
            .distanceOperation(aggregationOperations)
    }

    return aggregationOperations
}
