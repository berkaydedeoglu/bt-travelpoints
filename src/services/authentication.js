import UserModel from '../models/user'
import { compareSafely, encryptSafely } from '../helpers/passwordHelper'
import { execDBQuery, saveDBModel } from '../helpers/dbHelper'

export async function createUser ({ email, passwd, name, surname }) {
    const hashedPasswd = await encryptSafely(passwd)
    if (!hashedPasswd) { return false }

    const user = new UserModel({
        name: name,
        surname: surname,
        email: email,
        passwd: hashedPasswd
    })

    const saved = saveDBModel(user)
    if (!saved) { return false }

    return {
        name: user.name,
        surname: user.surname,
        email: user.email
    }
}

export async function authenticateUser (email, passwd) {
    // using findone and projections for performance
    const query = UserModel.findOne({ email: email })
    const user = await execDBQuery(query)

    if (!user) { return false }

    const authenticated = await compareSafely(passwd, user.passwd)
    if (!authenticated) { return false }

    return user
}

export async function removeUser (email) {
    const query = UserModel.findOneAndRemove({ email: email })
    const queryResponse = execDBQuery(query)
    if (!queryResponse) { return false }

    return queryResponse
}
