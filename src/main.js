import express from 'express'
import { load } from './loaders/main'

async function run () {
    const expressApp = express()
    await load(expressApp)
    expressApp.listen(process.env.API_PORT, () => console.log('Api is running'))
}

run()
