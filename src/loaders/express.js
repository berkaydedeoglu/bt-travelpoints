import express from 'express'
import routes from '../routes/main'
import authMiddleware from '../helpers/middlewares/authentication'

export function loadExpressRouters (app) {
    // Health Check endpoints
    app.get('/ping', (req, res) => {
        res.status(200)
            .send('pong')
            .end()
    })

    // Transforms the raw string of req.body into json
    app.use(express.json())

    // Handling JWT auth
    app.use(authMiddleware)

    // Loading all routes
    app.use(routes())
}
