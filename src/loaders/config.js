/**
 * This module loads .env file to
 * environment variables. Even though
 * this module contains only single line
 * of code, this was created as a seperated
 * module. Because we may should append
 * further logic in feature and as a more
 * imported one, this is a dependency and
 * should be imported with other dependencies.
 *
 * [Mostly before all of them]
 */
import dotenv from 'dotenv'

export function loadEnvVariables () {
    dotenv.config()
}
