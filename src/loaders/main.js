import { loadEnvVariables } from './config'
import { loadExpressRouters } from './express'
import database from './db'

/**
 * This function is loads/injects all the dependencies
 * at once an with a right order.
 *
 * @param {Express.app} -> an express app to load routers
 */
export async function load (app) {
    // matching all env variables
    loadEnvVariables()

    // connecting database
    await database()

    // Loading express routes
    loadExpressRouters(app)
}
