import mongoose from 'mongoose'

mongoose.Promise = global.Promise
export default async function connectDb () {
    const dbHost = process.env.MONGODB_HOST
    const dbName = process.env.MONGODB_DATABASE
    const dbUserName = process.env.MONGODB_UNAME
    const dbUserPasswd = process.env.MONGODB_PASSWD

    try {
        await mongoose.connect(`mongodb+srv://${dbUserName}:${dbUserPasswd}@${dbHost}/${dbName}`)
        console.log('Connected To DB Succesfully')
    } catch (err) {
        console.log(err)
    }
}
