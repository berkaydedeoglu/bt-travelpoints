import mongoose from 'mongoose'

const pointSchema = new mongoose.Schema({
    lat: { type: Number, required: true, min: -180, max: 180 },
    lon: { type: Number, required: true, min: -90, max: 90 }
})

export default pointSchema
