import mongoose from 'mongoose'
import pointSchema from './point'
import carSchema from './car'

const travelSchema = new mongoose.Schema({
    startingPoint: { type: pointSchema, required: true },
    destinitionPoint: { type: pointSchema, required: true },
    startingTime: { type: Date, required: true },
    destinitionTime: { type: Date, required: true },

    // Distance value can be calculated anytime but it will
    // used for querying. Thats this is preffered as required
    // instead of calculating every request
    distance: { type: Number, required: true, index: true },
    carDetails: { type: carSchema }
})

export default mongoose.model('travel', travelSchema, 'travels')
