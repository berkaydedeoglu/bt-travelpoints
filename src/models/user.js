import mongoose from 'mongoose'

const userSchema = new mongoose.Schema({
    name: String,
    surname: String,
    email: { type: String, required: true, unique: true, index: true },
    passwd: { type: String, required: true }
})

export default mongoose.model('user', userSchema, 'users')
