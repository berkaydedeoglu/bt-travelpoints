import mongoose from 'mongoose'

const carSchema = new mongoose.Schema({
    brand: String,
    model: String,
    modelYear: Number
})

export default carSchema
