import { getHaversineBorders } from '../../helpers/geolocationHelper'

function zoneOperation (operators, zone) {
    const basedLocation = { lat: zone.lat, lon: zone.lon }
    const edgeLocations = getHaversineBorders({
        basedLocation: basedLocation,
        radius: zone.radius
    })

    operators.push({
        $match: {
            'startingPoint.lat': {
                $gte: edgeLocations.bottom.lat,
                $lte: edgeLocations.top.lat
            },
            'startingPoint.lon': {
                $gte: edgeLocations.left.lon,
                $lte: edgeLocations.right.lon
            }
        }
    })

    return operators
}

function carGroupOperation (operators) {
    operators.push({
        $group: {
            _id: '$carDetails.modelYear',
            count: { $sum: 1 }
        }
    })

    return operators
}

function timeIntervalOperation (operators, timeInterval) {
    operators.push({
        $match: {
            startingTime: { $gte: timeInterval.start, $lte: timeInterval.end }
        }
    })

    return operators
}

function distanceOperation (operators) {
    operators.push({
        $group: {
            _id: null,
            max_distance: { $max: '$distance' },
            min_distance: { $min: '$distance' }
        }
    })
    return operators
}

export default {
    zoneOperation,
    carGroupOperation,
    timeIntervalOperation,
    distanceOperation
}
