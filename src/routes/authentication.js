import { Router } from 'express'
import * as authenticationService from '../services/authentication'
import * as jwtHelper from '../helpers/jwtHelper'
import { parseBasicAuthHeader, scrapeJWTTokenFromHeader } from '../helpers/httpHelper'

const router = new Router()
export default (app) => {
    app.use('/auth', router)

    router.post('/register', async (req, res) => {
        const user = await authenticationService.createUser({
            email: req.body.email,
            name: req.body.name,
            surname: req.body.surname,
            passwd: req.body.passwd
        })

        if (!user) {
            return res.status(406).json({ error: true })
        }

        const jwtToken = jwtHelper.createTokenForUser(user)
        if (!jwtToken) {
            return res.status(500).json({ error: true })
        }

        return res.status(200)
            .set('Authorization', `Bearer ${jwtToken}`)
            .json(user)
    })

    router.post('/login', async (req, res) => {
        const basicAuthHeader = req.get('Authorization')
        if (!basicAuthHeader) {
            return res.status(406).json({ error: true })
        }

        const [email, passwd] = parseBasicAuthHeader(basicAuthHeader)
        if (!email || !passwd) {
            return res.status(406).json({ error: true })
        }

        const user = await authenticationService.authenticateUser(email, passwd)
        if (!user) {
            return res.status(406).json({ error: true })
        }

        const jwtToken = jwtHelper.createTokenForUser(user)
        if (!jwtToken) {
            return res.status(500).json({ succes: false })
        }

        return res.status(200)
            .set('Authorization', `Bearer ${jwtToken}`)
            .json({ succes: true })
    })

    router.get('/renew-token', (req, res) => {
        const authHeader = req.get('Authorization')
        if (!authHeader) {
            return res.status(401).json({ error: true })
        }

        const jwtToken = scrapeJWTTokenFromHeader(authHeader)
        if (!jwtToken) {
            return res.status(401).json({ error: true })
        }

        const tokenBody = jwtHelper.verifyToken(jwtToken)
        const newToken = tokenBody ? jwtHelper.extendToken(tokenBody) : null

        if (!newToken) {
            return res.status(401).json({ error: true })
        }

        return res.status(200)
            .set('Authorization', `bearer ${jwtToken}`)
            .json({ succes: true })
    })
}
