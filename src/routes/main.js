import { Router } from 'express'
import authentication from './authentication'
import travel from './travel'

// Dependencies will be injected
export default () => {
    const app = Router()

    authentication(app)
    travel(app)

    return app
}
