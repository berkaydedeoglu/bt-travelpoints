import { Router } from 'express'
import { getTravels } from '../services/travel'

const router = new Router()

export default (app) => {
    app.use('/travel', router)

    router.get('/all', async (req, res) => {
        // TODO: This is a reporting api. That means this enpoint
        // will be hard loaded. That's why i should implement the paging
        // at least for this endpoint
        const { lat, lon, radius, startDate, endDate, groupByCarModelYear } = req.query
        if (!lat || !lon || !radius) {
            return res.status(400).json({ error: true })
        }

        const travels = await getTravels({
            zone: { lat: lat, lon: lon, radius: radius },
            timeInterval: startDate && endDate ? { start: startDate, end: endDate } : null,
            groupByCarModelYear: !!groupByCarModelYear
        })

        if (!travels) {
            return res.status(500).json({ error: true })
        }

        return res.status(200)
            .json(travels)
    })

    router.get('/distancePeaks', async (req, res) => {
        // Ignoring the code duplications. Because they have different
        // logics that's why i want to make them independent
        const { lat, lon, radius } = req.query
        if (!lat || !lon || !radius) {
            return res.status(400).json({ error: true })
        }

        const travels = await getTravels({
            zone: { lat: lat, lon: lon, radius: radius },
            distancePeaks: true
        })

        if (!travels) {
            return res.status(500).json({ error: true })
        }

        return res.status(200)
            .json(travels)
    })
}
