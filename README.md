# BT-TRAVELPOINTS
A service that holds travel data and provide them efficiently

# How can i improve
- PERSONAL TIME MANAGEMENT :(
- Fill all comments
- Enforce the tests. Add more edge case, refactor
- More secure JWT. Maybe JWS or JWE
- Check types at least at routes maybe i can use a library like celebrate
- Use a Dependency Injector for non-export functions
- Use a 3rd party logger like winston

# How to run
Please ensure that you have a running Mongo instance and a valid dotenv file.
You can find an example dotenv file in sources which named `example.env`.

```
$ npm install
$ npm run app
```


## Code Structure
You can start to review the code from /src file.

`main.js` is starting point of the app

Here is a brief structure summary for this service

- **helpers:** Contains some helper functions or decorators. Mostly the service layer are using that functions
- **loaders:** Contains some modules that should be loaded on start up. Also this layer managing some dependecies
- **models:** Contains DB models
- **routers:** The endpoint codes. Note that the endpoints does not contains any bussiness logics inside itselfs. They passes the logics to service layer.
- **services:** Mostly includes bussiness logic codes. This layer is a bridge between data layer (models) and routes.

# Api Usage
This service communicates with the outside via REST standarts. So you should use JSON while communicating
## /ping
Checks the service status
```
Request
=======
GET http://{{host}}:{{port}}/ping


Response
========
pong

```

## /auth
BT-TravelPoints service is using JWT as a token.
### [POST] /register 
Registering a user to the system. There is no need to relogin after registering the system. The provided token can be used.

```
REQUEST
=======
POST http://{{host}}:{{port}}/auth/register
Content-Type: application/json

{
    "name": "Berkay",
    "surname": "Dedeoglu",
    "email": "ber@de.com",
    "passwd": "12345678"
}

---

RESPONSE
========
Content-Type: application/json; charset=utf-8
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImJlckBkZS5jb20iLCJleHAiOjE2NDYwMzMyNzgsImlhdCI6MTY0NjAzMTc3OH0.lCO22H9UacYG6kxcrAD34UGC-ZFozHO4_luXL_iDF0k
Body

{"name":"Berkay","surname":"Dedeoglu","email":"ber@de.com"}
```



### [POST] /login
Logging in the system. The basic authorization model is used on this endpoint. Plesa see `RFC-7617`. The fetched token can be used on other requests.

```
REQUEST
=======
POST http://{{host}}:{{port}}/auth/login
Authorization: Basic YmVyQGRlLmNvbToxMjM0NTY3OA==
Content-Type: application/json


RESPONSE
========

Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImJlckBkZS5jb20iLCJleHAiOjE2NDYwMzQwNzksImlhdCI6MTY0NjAzMjU3OX0._sG6Rv99oCtuo2WP7m-4mDF5DrQQaCToNE3V-rnifTM
Content-Type: application/json; charset=utf-8
Body:

{"succes":true}

```
### Renewing the jwt
On every endpoint calls (except /login and /register) a valid JWT token is required.
Every token is valid in 25 mins (editable on .env file) from it's creation.

We are providing new token on new requests (this will be changed). So new tokens can
be used on every request.

## Travel
The BT-TravelPoints service is used to get some reports user trips. Note that the zone parameters are always required.
### Adding a travel
**Not implemented yet**

### Get Travels
Gets the system travels according to given parameters. Note that the zone parameters
are always required.

lat: latitude in degrees
lon: longtitude in degrees
radius: radius of virtual circle. In KM

**Get Travels With Given Zone**
```
REQUEST
=======

GET http://{{host}}:{{port}}/travel/all
QueryParam lat=36&lon=42&radius=10
Authorization: Bearer <JWT Token>


RESPONSE
========

Authorization: Bearer <JWT Token>
Content-Type: application/json; charset=utf-8
Body:

[{"_id":"621c472535ad9b1227c27e43","startingPoint":{"lat":36,"lon":42},"destinitonPoint":{"lat":36.32,"lon":42.53},"startingTime":12345,"destinitionTime":3456,"distance":4,"carDetails":{"modelYear":1985}}]
```

**Get Travels With Given Zone And Date**
```
REQUEST
=======

GET http://{{host}}:{{port}}/travel/all
QueryParam lat=36&lon=42&radius=10&startDate=12345&endDate=1234567
Authorization: Bearer <JWT Token>


RESPONSE
========

Authorization: Bearer <JWT Token>
Content-Type: application/json; charset=utf-8
Body:

[{"_id":"621c472535ad9b1227c27e43","startingPoint":{"lat":36,"lon":42},"destinitonPoint":{"lat":36.32,"lon":42.53},"startingTime":12345,"destinitionTime":3456,"distance":4,"carDetails":{"modelYear":1985}}]
```

**Get Travel Count By CarModelYear**
```
REQUEST
=======

GET http://{{host}}:{{port}}/travel/all
QueryParam lat=36&lon=42&groupByCarModelYear=true&radius=1000
Authorization: Bearer <JWT Token>


RESPONSE
========

Authorization: Bearer <JWT Token>
Content-Type: application/json; charset=utf-8
Body:

[{"_id":1985,"count":1}]

```

### Get Distance Peaks for A Zone
You also can get the peak distance values via this service

```
REQUEST
=======

GET http://{{host}}:{{port}}/travel/distancePeaks
QueryParam lat=36&lon=42&radius=10
Authorization: Bearer <JWT Token>


RESPONSE
========

Authorization: Bearer <JWT Token>
Content-Type: application/json; charset=utf-8
Body:

[{"_id":null,"max_distance":4,"min_distance":4}]

```




# Apologize :(
I am sorry because of i haven't finalized this little task in time. There are still missing parts.

**Test Link:** [ec2-18-159-206-0.eu-central-1.compute.amazonaws.com:80](ec2-18-159-206-0.eu-central-1.compute.amazonaws.com)

You can fetch the test data by using this query
``` 
http://ec2-18-159-206-0.eu-central-1.compute.amazonaws.com:80/travel/all?lat=36&lon=42&radius=1000
```