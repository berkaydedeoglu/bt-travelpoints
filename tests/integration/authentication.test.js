/* eslint-disable dot-notation */
/* eslint-disable no-undef */

import supertest from 'supertest'
import 'dotenv/config'
import jsonwebtoken from 'jsonwebtoken'
import * as testHelpers from './testHelpers'

let app = null

beforeAll(() => {
    app = testHelpers.createAnExpressApp()
})

beforeEach(async () => {
    // We are not clearing the database because
    // we have only one database :)

    await testHelpers.removeTestUser()
})

afterAll(async () => {
    await testHelpers.removeTestUser()
})

describe('Registering Endpoints', () => {
    test('Registering with empty body', async () => {
        const response = await supertest(app)
            .post('/auth/register')
            .set('Accept', 'application/json')
            .send()
        expect(response.status).toEqual(406)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toEqual(true)
    })

    test('Create an user in proper way', async () => {
        const response = await supertest(app)
            .post('/auth/register')
            .set('Accept', 'application/json')
            .send({
                name: testHelpers.USER_NAME,
                surname: testHelpers.USER_SURNAME,
                email: testHelpers.USER_EMAIL,
                passwd: testHelpers.USER_PASSWORD
            })

        const { authType, jwtToken } = response.headers['Authorization'].split(' ')
        const decodedJWT = jsonwebtoken.verify(jwtToken, process.env.JWT_SECRET)

        expect(response.status).toEqual(200)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.succes).toBeTruthy()
        expect(authType).toEqual('bearer')
        expect(decodedJWT).toMatchObject({
            name: testHelpers.USER_NAME,
            surname: testHelpers.USER_SURNAME,
            email: testHelpers.USER_EMAIL
        })
    })

    test('Registering an existing user', async () => {
        await testHelpers.createMockUser()

        const response = await supertest(app)
            .post('/auth/register')
            .set('Accept', 'application/json')
            .send({
                name: testHelpers.USER_NAME,
                surname: testHelpers.USER_SURNAME,
                email: testHelpers.USER_EMAIL,
                passwd: testHelpers.USER_PASSWORD
            })

        expect(response.status).toEqual(409)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toEqual(true)
    })
})

describe('User Authentication Endpoints', () => {
    test('Trying to login without credentials', async () => {
        const response = testHelpers.login({
            expressApp: app,
            email: null,
            passwd: null
        })
        expect(response.status).toEqual(406)
    })

    test('Logging in succesfully', async () => {
        await testHelpers.createMockUser()

        const response = testHelpers.login({
            expressApp: app,
            email: testHelpers.USER_EMAIL,
            passwd: testHelpers.USER_PASSWORD
        })

        expect(response.status).toEqual(200)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.succes).toBeTruthy()
        expect(authType).toEqual('bearer')
        expect(decodedJWT).toMatchObject({
            name: testHelpers.USER_NAME,
            surname: testHelpers.USER_SURNAME,
            email: testHelpers.USER_EMAIL
        })
    })

    test('Logging in with wrong password', async () => {
        await testHelpers.createMockUser()

        const response = testHelpers.login({
            expressApp: app,
            email: testHelpers.USER_EMAIL,
            passwd: 'wrongPassword'
        })

        expect(response.status).toEqual(406)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toBeTruthy()
    })

    test('Trying to login with unregistered credentials', async () => {
        const response = testHelpers.login({
            expressApp: app,
            email: 'unregistered@test.com',
            passwd: 'wrongPassword'
        })

        expect(response.status).toEqual(401)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toBeTruthy()
    })
})

describe('Token processes', () => {
    test('Token Expiring', async () => {
        await testHelpers.createMockUser()

        const firstResponse = testHelpers.login({
            expressApp: app,
            email: testHelpers.USER_EMAIL,
            passwd: testHelpers.USER_PASSWORD
        })
        const firstJWTToken = firstResponse.headers['Authorization'].split(' ')[1]

        const jwtExpiringTime = process.env.JWT_EXPIRING_TIME
        process.env.JWT_EXPIRING_TIME = 0

        const secondResponse = testHelpers.login({
            expressApp: app,
            email: testHelpers.USER_EMAIL,
            passwd: testHelpers.USER_PASSWORD
        })

        process.env.JWT_EXPIRING_TIME = jwtExpiringTime
        const secondJWTToken = secondResponse.headers['Authorization'].split(' ')[1]

        expect(response.status).toEqual(401)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toBeTruthy()
        expect(firstJWTToken).not.toEqual(secondJWTToken)
    })

    test('Renew Token', async () => {
        await testHelpers.createMockUser()

        const loginResponse = testHelpers.login({
            expressApp: app,
            email: testHelpers.USER_EMAIL,
            passwd: testHelpers.USER_PASSWORD
        })

        const firstJWTToken = loginResponse.headers['Authorization'].split(' ')[1]

        const renewTokenResponse = await supertest(app)
            .get('/auth/renew-token')
            .set('Authorization', firstJWTToken)

        const secondJWTToken = renewTokenResponse.headers['Authorization'].split(' ')[1]
        expect(response.status).toEqual(200)
        expect(firstJWTToken).not.toEqual(secondJWTToken)
    })

    test('Trying to renew with wrong token', async () => {
        const response = await supertest(app)
            .get('/auth/renew-token')
            .set('Authorization', 'WrongJWTToken')
            .send()

        expect(response.status).toEqual(401)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toBeTruthy()
    })

    test('Trying to renew without token', async () => {
        const response = await supertest(app)
            .get('/auth/renew-token')
            .send()

        expect(response.status).toEqual(401)
        expect(response.headers['Content-Type']).toMatch(/json/)
        expect(response.body.error).toBeTruthy()
    })
})
