/* eslint-disable no-undef */

import express from 'express'
import supertest from 'supertest'
import 'dotenv/config'
import * as testHelpers from './testHelpers'

// Preparing the api before testing
const app = express() // TODO remove it

let travel1, travel2, travel3, travel4, travel5
let jwtToken

beforeAll(async () => {
    await testHelpers.createMockUser()
    await testHelpers.clearTestUserData()

    travel1 = await testHelpers.createTestTravelData({
        startLocation: { lat: 30.8773, lon: 42.9897 },
        endLocation: { lat: 36.5424, lon: 39.4352 },
        travelDetails: { vehicleModel: 2012, startDate: 1645577531, endDate: 1645591931 }
    })

    travel2 = await testHelpers.createTestTravelData({
        startLocation: { lat: 30.8777, lon: 42.989 },
        endLocation: { lat: 30.888, lon: 42.3049 },
        travelDetails: { vehicleModel: 2012, startDate: 1644638412, endDate: 1644642032 }
    })

    travel3 = await testHelpers.createTestTravelData({
        startLocation: { lat: 30.5671, lon: 43.034 },
        endLocation: { lat: 36, lon: 21 },
        travelDetails: { carModel: 2011, startDate: 1641902401, endDate: 1641907261 }
    })

    travel4 = await testHelpers.createTestTravelData({
        startLocation: { lat: -30.8773, lon: 42.9897 },
        endLocation: { lat: -29.3524, lon: 42.9098 },
        travelDetails: { carModel: 2000, startDate: 1641926521, endDate: 1641929581 }
    })

    travel5 = await testHelpers.createTestTravelData({
        startLocation: { lat: -30.8671, lon: 43.034 },
        endLocation: { lat: 36.3426, lon: 43.0933 },
        travelDetails: { carModel: 2020, startDate: 1641936222, endDate: 1642015241 }
    })

    const loginResponse = testHelpers.login({
        expressApp: app,
        email: testHelpers.USER_EMAIL,
        passwd: testHelpers.USER_PASSWORD
    })

    // Fully relying the login
    jwtToken = loginResponse.headers['Authorization'].split(' ')[1] // eslint-disable-line dot-notation
})

afterAll(async () => {
    await testHelpers.clearTestUserData()
    await testHelpers.removeMockUser()
})

describe('Get Travels', () => {
    test('Empty location points', async () => {

    })

    test('Empty response if not found', async () => {

    })

    test('Able to query all trips which are started in a region specified by a point', async () => {

    })

    test('Query the trips by zone with dates', async () => {

    })

    test('Getting maximum distance travelled by a zone', async () => {

    })
})
