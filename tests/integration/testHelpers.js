import { createUser, removeUser } from '../../src/services/authentication'
import { removeAllTravelPointsForAUser, addATravelPoint } from '../../src/services/travelPoints'
import supertest from 'supertest'
import express from 'express'
import 'dotenv/config'
import { load } from '../../src/loaders/main'

// Test user data
export const USER_NAME = 'testName'
export const USER_SURNAME = 'testSurname'
export const USER_EMAIL = 'test@test.com'
export const USER_PASSWORD = 'testPassword'

/**
 * Creates a mock user using user service
 * functions. This is relying createUser service
 * function is working properly
 */
export async function createMockUser () {
    await createUser({
        name: USER_NAME,
        surname: USER_SURNAME,
        email: USER_EMAIL,
        passwd: USER_PASSWORD
    })
}

/**
 * Removes the mock user using the related
 * user service function. This is relying removeUser service
 * function is working properly
 */
export async function removeTestUser () {
    await removeUser(USER_EMAIL)
}

/**
 * Removes all the travel point data for the
 * test user.
 */
export async function clearTestUserData () {
    await removeAllTravelPointsForAUser(USER_EMAIL)
}

/**
 * Creates a base64 authorisation token for HTTP basic
 * authentication. Pls see RFC-7617
 */
export function createBasicAuthToken ({ email, passwd }) {
    const loginString = `${email}:${passwd}`
    return Buffer.from(loginString).toString('base64')
}

/**
 * Logins the given system via provided credetials
 * @param {object} param -> parameter object
 * @param {Express} param.expressApp -> Express.js app object
 * @param {string} param.email -> login string, user email, user id
 * @param {string} param.passwd -> user password
 * @returns {(supertest.Response)} -> A supertest response if logged in
 *                                    successfully else null
 */
export async function login ({ expressApp, email, passwd }) {
    const authString = createBasicAuthToken({
        email: email,
        passwd: passwd
    })

    return await supertest(expressApp)
        .post('/auth/login')
        .set('Authorization', `Basic ${authString}`)
        .send()
}

/**
 * Creates a travel data for test user
 * @param {object} param -> Travel object
 * @param {LocationObject} param.startLocation -> see LocationObject in services/travelPoints.js
 * @param {LocationObject} param.finishLocation -> see LocationObject in services/travelPoints.js
 * @param {TravelDetails} param.travelDetails ->  see TravelDetails in services/travelPoints.js
 */
export async function createTestTravelData ({ startLocation, endLocation, travelDetails }) {
    await addATravelPoint({
        email: USER_EMAIL,
        startLocation: startLocation,
        endLocation: endLocation,
        travelDetails: travelDetails
    })
}

export function createAnExpressApp () {
    const expressApp = express()
    load(expressApp)
    return expressApp
}
